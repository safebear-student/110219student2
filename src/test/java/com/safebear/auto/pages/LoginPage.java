package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import com.safebear.auto.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle(){
        return driver.getTitle();
    }

    public void enterUsername(String username){
        Utils.waitForElement(locators.getUsernameLocator(),driver).sendKeys(username);
    }

    public void enterPassword(String password){
        Utils.waitForElement(locators.getPasswordFieldLocator(),driver).sendKeys(password);
    }

    public void clickLoginButton(){
        // driver.findElement(locators.getLoginButtonLocator()).click();
        Utils.waitForElement(locators.getLoginButtonLocator(),driver).click();
    }

    public String checkForFailedLoginWarning(){
        // return driver.findElement(locators.getFailedLoginMessage()).getText();
        return Utils.waitForElement(locators.getFailedLoginMessage(),driver).getText();

    }
}
