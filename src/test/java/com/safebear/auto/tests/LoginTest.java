package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class LoginTest {

    //getting our driver e.g. ChromeDriver
    WebDriver driver = Utils.getDriver();

    //connecting to our pages - creating objects of the pages
    LoginPage loginPage = new LoginPage(driver);
    ToolsPage toolsPage = new ToolsPage(driver);

    @AfterTest
    public void teardown(){
        driver.quit();
    }

    //Test fotr invalid login

    //Test for valid login
    @Test
    public void testValidLogin(){

        //go to login page
        driver.get(Utils.getUrl());

        //enter valid login details
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");

        //click on login
        loginPage.clickLoginButton();

        //aassert message is correct
        Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains("Login Successful"));

    }


    @Test
    public void  testInvalidLogin(){

        //go to login page
        driver.get(Utils.getUrl());

        //enter valid login details
        loginPage.enterUsername("badtester");
        loginPage.enterPassword("dontletmein");

        //click on login
        loginPage.clickLoginButton();

        //assert message is correct
        Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains("Username or Password is incorrect"));

    };

}

