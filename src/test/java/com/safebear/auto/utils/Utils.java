package com.safebear.auto.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class Utils {

    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");
    //private static final String URL = System.getProperty("url", "https://bbc.co.uk");
    //private static final String URL = System.getProperty("url", "https://www.twinings.co.uk");

    private static final String BROWSER = System.getProperty("browser", "chrome");

    public static String getUrl() {
        return URL;
    }

    public static WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");
        //options.addArguments("window-size=650,375");
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);
            case "firefox":
                return new FirefoxDriver();
            case "headless":
                options.addArguments("headless", "disable-gpu");
                return new ChromeDriver(options);
            default:
                return new ChromeDriver(options);

        }
    }

    public static WebElement waitForElement(By locator, WebDriver driver) {

        //This is where we set the timeout value (of 5 seconds)
        WebDriverWait wait = new WebDriverWait(driver, 5);

        WebElement element;

        try {
            //Try to find the element straight away
            element = driver.findElement(locator);

        } catch (Exception e) {
            //If it still isn't there, print a stack trace and take a screenshot
            System.out.println(e.toString());
            capturescreenshot(driver, generateScreenShotFileName());
            throw (e);
        }
        //return the web element if it's been found
        return element;
    }

    public static String generateScreenShotFileName() {
        //create filename
        return new SimpleDateFormat("yyy.MM.dd.HH.mm.ss").format(new Date()) + ".png";
    }

    public static void capturescreenshot(WebDriver driver, String filename) {
        //take screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        //Make sure that the "screenshots" directory exists
        File file = new File("target/screenshots");
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Failed to create directory!");
            }
        }

        //copy file to filename and location we set before
       try {
            copy(scrFile, new File("target/screenshots/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
