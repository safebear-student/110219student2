Feature: tea
  In order to display a list of tea on the Twinings webpage

  Scenario: Display the teas list
    When I browse to the Twinings page
    Then I can see a list of teas
